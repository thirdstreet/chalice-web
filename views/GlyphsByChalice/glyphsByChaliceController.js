/**
 * Created by damonfloyd on 6/5/15.
 */
(function() {

    function GlyphsByChaliceController($state, $log, $global, $chaliceService) {
        $log.module = 'GlyphsByChalice';
        this.chalices = [];
        this.areas = [
            { name: 'Pthumeru', id: 1 },
            { name: 'Hintertomb', id: 2 },
            { name: 'Loran', id: 3 },
            { name: 'Isz', id: 4 }
        ]
        var _this = this;

        $chaliceService.getWithGlyphs().then(function(response) {
            _this.chalices = response.data;
            //$log.log(_this.chalices[0]);
        });

        this.showChaliceGlyphs = function(chaliceId) {
            $global.current().chalice.chaliceId = chaliceId;
            $global.current().searchType = 'ByChaliceId';
            $state.transitionTo('GlyphSearchResults');
        }
    }

    angular.module('chalice.controllers')
        .config(function($stateProvider) {
            $stateProvider
                .state('GlyphsByChalice', {
                    url: '/bychalice',
                    templateUrl: 'views/GlyphsByChalice/glyphsByChalice.html'
                });
        })
        .controller('GlyphsByChaliceController',
        ['$state', 'Log', 'GlobalState', 'ChaliceService', GlyphsByChaliceController]);

})();