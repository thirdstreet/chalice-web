/**
 * Created by damonfloyd on 6/9/15.
 */
(function() {

    function AddGlyphFeatureController($timeout, $state, $log, $global, $featureService, $glyphService) {
        $log.module = 'AddGlyphFeatureController';

        var _this = this;
        this.glyph = $global.current().glyph;
        $log.log(this.glyph);
        this.glyph.glyphId = $global.current().glyph.glyphId;

        if(this.glyph.glyphId === undefined) {
            $state.transitionTo('GlyphsByFeature');
            return;
        }

        this.notes = "";
        this.feature = {};
        this.featureList = [];
        this.glyphFeatures = [];
        this.featureAdded = false;
        this.featureError = '';
        this.fadeIt = false;


        if($global.current().justAdded === 'Glyph') {
            this.addedMessage = 'Glyph added.';
        }

        this.layerList = [
            { name: 1, value: 1 },
            { name: 2, value: 2 },
            { name: 3, value: 3 },
            { name: 4, value: 4 }
        ];

        this.layer = this.layerList[0];

        this.submittedBy = $global.current().submittedBy;
        this.editPass = $global.current().editPass;

        var _this = this;

        $featureService.getAll().then(function(response) {
            var feats = response.data[0];
            angular.forEach(feats, function (f) {
                var opt = {
                    value: f.FeatureId,
                    group: f.TypeName,
                    name: f.FeatureName
                };
                _this.featureList.push(opt);
            });
            _this.feature = _this.featureList[0];
        });

        $featureService.glyphFeatures(this.glyph.glyphId).then(function(response) {
            $log.log(response.data[0]);
            angular.forEach(response.data[0], function(f) {
                var feature = {
                    id: f.id,
                    name: f.name,
                    layer: f.layer
                };
                _this.glyphFeatures.push(feature);
            });
        });

        this.goToBosses = function() {
            $state.transitionTo('AddGlyphBoss');
        };

        this.goBackToEdit = function() {
            $state.transitionTo('EditGlyph');
        };

        this.reset = function(form) {
            if(form) {
                form.$setPristine();
                form.$setUntouched();
            }
            _this.notes = '';
            _this.layer = _this.layerList[0];
            _this.feature = _this.featureList[0];
        };

        this.addFeature = function() {
            var postData = {
                featureId: _this.feature.value,
                layer: _this.layer.value,
                notes: _this.notes,
                editPass: _this.editPass
            };
            $log.log(postData);
            $glyphService.addFeatureToGlyph(_this.glyph.glyphId, postData)
                .success(function (response) {
                    var msg = response[0][0].Message;
                    var id = response[0][0].id;
                    if(msg === 'Glyph Feature added.') {
                        _this.addedMessage = 'Feature added.';
                        _this.fadeIt = false;
                        // add new feature to display list
                        var f = {
                            id: id,
                            name: _this.feature.name,
                            layer: postData.layer
                        };
                        _this.glyphFeatures.unshift(f);
                    }
                    else {
                        alert(msg);
                    }
                })
                .error(function (failure) {
                    _this.featureError = 'Something bad happened.  Try again later.';
                });

        };

        this.deleteFeature = function(featureId) {
            var postData = {
                editPass: _this.editPass
            };
            $log.log(postData);
            $featureService.deleteGlyphFeature(featureId, postData).then(function(response) {
                var msg = response.data[0][0].Message;
                var id = response.data[0][0].id;
                if(msg === "Feature not found or password incorrect!") {
                    alert(msg);
                }
                else {
                    // remove item from local array
                    var idx = -1;
                    for(var i = 0; i < _this.glyphFeatures.length; i++) {
                        var f = _this.glyphFeatures[i];
                        if(f.id === featureId) {
                            idx = i;
                        }
                    }
                    if(idx !== -1) {
                        _this.glyphFeatures.splice(idx, 1);
                    }
                }
            });
        };

    }

    angular.module('chalice.controllers')
        .config(function($stateProvider) {
            $stateProvider
                .state('AddGlyphFeature', {
                    url: '/addglyphfeature',
                    templateUrl: 'views/AddGlyphFeature/addGlyphFeature.html'
                });
        })
        .controller('AddGlyphFeatureController',
            [
                '$timeout',
                '$state',
                'Log',
                'GlobalState',
                'FeatureService',
                'GlyphService',
                AddGlyphFeatureController
            ]
    );

})();