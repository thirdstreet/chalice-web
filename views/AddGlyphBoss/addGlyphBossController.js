/**
 * Created by damonfloyd on 7/31/15.
 */
(function() {

    function AddGlyphBossController($timeout, $state, $log, $global, $glyphService, $bossService) {
        $log.module = 'AddGlyphBossController';

        var _this = this;
        this.glyph = $global.current().glyph;
        $log.log(this.glyph);
        this.glyph.glyphId = $global.current().glyph.glyphId;

        if(this.glyph.glyphId === undefined) {
            $state.transitionTo('GlyphsByFeature');
            return;
        }

        this.notes = "";
        this.boss = {};
        this.bossList = [];
        this.layerBosses = [];
        this.bossAdded = false;
        this.bossError = '';
        this.fadeIt = false;


        if($global.current().justAdded === 'Glyph') {
            this.addedMessage = 'Glyph added.';
        }

        this.layerList = [
            { name: 1, value: 1 },
            { name: 2, value: 2 },
            { name: 3, value: 3 },
            { name: 4, value: 4 }
        ];

        this.layer = this.layerList[0];

        this.submittedBy = $global.current().submittedBy;
        this.editPass = $global.current().editPass;

        var _this = this;

        $bossService.getAll().then(function(response) {
            var bosses = response.data[0];
            $log.log(bosses);
            _this.bossList = bosses;
            //angular.forEach(feats, function (f) {
            //    var opt = {
            //        value: f.FeatureId,
            //        group: f.TypeName,
            //        name: f.FeatureName
            //    };
            //    _this.bossList.push(opt);
            //});
            _this.boss = _this.bossList[0];
        });


        this.reset = function(form) {
            if(form) {
                form.$setPristine();
                form.$setUntouched();
            }
            _this.notes = '';
            _this.layer = _this.layerList[0];
            _this.boss = _this.bossList[0];
        };

        this.goToFeatures = function() {
            $state.transitionTo('AddGlyphFeature');
        };

        this.addBoss = function() {
            var postData = {
                bossId: _this.boss.value,
                layer: _this.layer.value,
                notes: _this.notes,
                editPass: _this.editPass
            };
            $log.log(postData);
            $glyphService.addBossToGlyph(_this.glyph.glyphId, postData)
                .success(function (response) {
                    var msg = response[0][0].Message;
                    var id = response[0][0].id;
                    if(msg === 'Glyph Boss added.') {
                        _this.addedMessage = 'Boss added.';
                        _this.fadeIt = false;
                        // add new boss to display list
                        var f = {
                            id: id,
                            name: _this.boss.name,
                            layer: postData.layer
                        };
                        _this.layerBosses.unshift(f);
                    }
                    else {
                        alert(msg);
                    }
                })
                .error(function (failure) {
                    _this.bossError = 'Something bad happened.  Try again later.';
                });

        };

        this.deleteBoss = function(bossId) {
            var postData = {
                editPass: _this.editPass
            };
            $log.log(postData);
            $featureService.deleteGlyphBoss(bossId, postData).then(function(response) {
                var msg = response.data[0][0].Message;
                var id = response.data[0][0].id;
                if(msg === "Boss not found or password incorrect!") {
                    alert(msg);
                }
                else {
                    // remove item from local array
                    var idx = -1;
                    for(var i = 0; i < _this.layerBosses.length; i++) {
                        var f = _this.layerBosses[i];
                        if(f.id === bossId) {
                            idx = i;
                        }
                    }
                    if(idx !== -1) {
                        _this.layerBosses.splice(idx, 1);
                    }
                }
            });
        };

    }

    angular.module('chalice.controllers')
        .config(function($stateProvider) {
            $stateProvider
                .state('AddGlyphBoss', {
                    url: '/addglyphboss',
                    templateUrl: 'views/AddGlyphBoss/addGlyphBoss.html'
                });
        })
        .controller('AddGlyphBossController',
        [
            '$timeout',
            '$state',
            'Log',
            'GlobalState',
            'GlyphService',
            'BossService',
            AddGlyphBossController
        ]
    );

})();