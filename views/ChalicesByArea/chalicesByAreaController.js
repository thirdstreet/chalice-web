/**
 * Created by damonfloyd on 6/2/15.
 */
(function() {

    function ChalicesByAreaController($state, $log, $global, $chaliceService) {
        var areaId = $global.current().areaId || 1;
        var _this = this;
        $chaliceService.getByArea(areaId).then(function(response) {
            $log.log(response.data);
            _this.area = {
                areaId: response.data[0].areaId,
                areaName: response.data[0].areaName,
                chalices: response.data
            };
        });

        this.showChaliceDetail = function(chaliceId) {
            $global.current().chalice.chaliceId = chaliceId;
            $state.transitionTo('ChaliceDetail');
        }
    }

    angular.module('chalice.controllers')
        .config(function($stateProvider) {
            $stateProvider
                .state('ChalicesByArea', {
                    url: '/chalicesbyarea',
                    templateUrl: 'views/ChalicesByArea/chalicesByArea.html'
                });
        })
        .controller('ChalicesByAreaController',
        [
            '$state',
            'Log',
            'GlobalState',
            'ChaliceService',
            ChalicesByAreaController
        ]);

})();