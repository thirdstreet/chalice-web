/**
 * Created by damonfloyd on 6/1/15.
 */

(function() {

    function GlyphDetailController($rootScope, $state, $log, $global, $glyphService) {

        $log.module = 'GlyphDetailController';
        var _this = this;
        _this.voted = false;

        var glyphId = $global.current().glyph.GlyphId || 1;
        $glyphService.getById(glyphId).then(function(response) {
            _this.glyph = response.data.glyph;
            if(response.data.glyph === undefined) {
                alert('Glyph Not Found!');
                $state.transitionTo('GlyphsByFeature');
                return;
            }
            _this.glyph.riteString = formatRites(_this.glyph);
            _this.glyph.workingPercentage = computeWorkingPercentage(_this.glyph.workingVotes, _this.glyph.nonWorkingVotes);

            //$log.log(_this.glyph.workingPercentage);

            _this.glyph.weapons = [];
            _this.glyph.bloodGems = [];
            _this.glyph.runes = [];
            _this.glyph.upgradeMats = [];
            _this.glyph.ritualMats = [];
            _this.glyph.special = [];
            _this.glyph.bosses = [];

            angular.forEach(_this.glyph.features, function(f) {
                if(f.featureId !== null) {
                    switch (f.featureType) {
                        case "Weapon":
                            _this.glyph.weapons.push(f);
                            break;
                        case "Blood Gem":
                            _this.glyph.bloodGems.push(f);
                            break;
                        case "Upgrade Material":
                            _this.glyph.upgradeMats.push(f);
                            break;
                        case "Ritual Material":
                            _this.glyph.ritualMats.push(f);
                            break;
                        case "Special":
                            _this.glyph.special.push(f);
                            break;
                        default:
                            _this.glyph.runes.push(f);
                            break;
                    }
                }
            });

            //$log.log(response.data.glyph);
            // get the boss list
            $glyphService.getBosses(glyphId).then(function(response) {
                $log.log(response.data[0]);
                if(typeof response.data[0][0] !== 'undefined') {
                    for(var i = 0;i < response.data[0].length; i++ ) {
                        _this.glyph.bosses.push(response.data[0][i]);
                    }
                }

                $log.log(_this.glyph);
            });
        });

        this.showChaliceDetail = function(chaliceId) {
            $global.current().chalice.chaliceId = chaliceId;
            $state.transitionTo('ChaliceDetail');
        };

        this.back = function() {
            var previous = $rootScope.previousState;
            $state.transitionTo(previous);
        };

        this.viewSubmitterGlyphs = function(submitter) {
            $global.current().searchType = 'GlyphsBySubmitter';
            $global.current().submitterSearch = submitter;
            $state.transitionTo('GlyphSearchResults');
        };

        this.verify = function(working) {
            $glyphService.voteOnGlyph(_this.glyph.glyphId, working).then(function() {
                _this.voted = true;
                if(working === 1) {
                    _this.glyph.workingVotes++;
                }
                else {
                    _this.glyph.nonWorkingVotes++;
                }

                _this.glyph.workingPercentage = computeWorkingPercentage(_this.glyph.workingVotes, _this.glyph.nonWorkingVotes);
            });
        };

        this.edit = function() {
            $state.transitionTo('EditGlyph');
        };

        function computeWorkingPercentage(working, nonworking) {
            //$log.log(working, nonworking);
            var total = working + nonworking;
            var percentage = (working / total) * 100;
            if(isNaN(percentage)) {
                percentage = 0;
            }
            return percentage;
        }

        function formatRites(glyph) {
            var riteStr = '';
            if(glyph.isFetid) {
                riteStr += 'Fetid';
            }
            if(glyph.isRotted) {
                if(riteStr !== '') {
                    riteStr += ', Rotted';
                }
                else {
                    riteStr = 'Rotted';
                }
            }

            if(glyph.isCursed) {
                if(riteStr !== '') {
                    riteStr += ', Cursed';
                }
                else {
                    riteStr = 'Cursed';
                }
            }

            if(glyph.isSinister) {
                if(riteStr !== '') {
                    riteStr += ', Sinister';
                }
                else {
                    riteStr = 'Sinister';
                }
            }

            if(riteStr === '') {
                riteStr = 'None.';
            }

            return riteStr;
        }


    }

    angular.module('chalice.controllers')
        .config(function($stateProvider) {
            $stateProvider
                .state('GlyphDetail', {
                    url: '/glyph',
                    templateUrl: 'views/GlyphDetail/glyphDetail.html'
                });
        })
        .controller('GlyphDetailController',
        ['$rootScope', '$state', 'Log', 'GlobalState', 'GlyphService', GlyphDetailController]);

})();