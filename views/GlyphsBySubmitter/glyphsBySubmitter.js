/**
 * Created by damonfloyd on 7/16/15.
 */
(function() {

    function GlyphsBySubmitterController($state, $log, $global) {


        this.submitterName = $global.current().submitterSearch || '';

        this.go = function() {
            $global.current().submitterSearch = this.submitterName;
            $global.current().searchType = 'GlyphsBySubmitter';
            $state.transitionTo('GlyphSearchResults');
        }
    }

    angular.module('chalice.controllers')
        .config(function($stateProvider) {
            $stateProvider
                .state('GlyphsBySubmitter', {
                    url: '/glyphsbysubmitter',
                    templateUrl: 'views/GlyphsBySubmitter/glyphsBySubmitter.html'
                }
            );
        })
        .controller('GlyphsBySubmitterController',
        ['$state', 'Log', 'GlobalState', GlyphsBySubmitterController]);

})();