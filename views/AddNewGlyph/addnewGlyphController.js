/**
 * Created by monk on 6/6/15.
 */
(function() {

    function AddNewGlyphController($state, $log, $global, $chaliceService, $glyphService) {

        $log.module = 'AddNewGlyph';
        var _this = this;

        this.newGlyph = {
            chalice: {},
            glyph: '',
            isFetid: 0,
            isRotted: 0,
            isCursed: 0,
            isSinister: 0,
            submittedBy: '',
            notes: '',
            editPass: ''
        };

        this.chalices = [];

        $chaliceService.getRoot().then(function(response) {
            //$log.log(response.data);
            angular.forEach(response.data, function(c) {
                var chalice = {
                    value: c.chaliceId,
                    areaName: c.areaName,
                    name: c.chaliceName
                };
                _this.chalices.push(chalice);
            });
            _this.newGlyph.chalice = _this.chalices[0];
            //$log.log(_this.chalices[0]);
        });

        this.reset = function(form) {
            if(form) {
                form.$setPristine();
                form.$setUntouched();
            }
            _this.newGlyph = {
                chalice: _this.chalices[0],
                glyph: '',
                isFetid: 0,
                isRotted: 0,
                isCursed: 0,
                isSinister: 0,
                submittedBy: 'AppTesting',
                notes: '',
                editPass: 'abcdef'
            };
        };

        this.doAdd = function() {
            var glyph = angular.copy(_this.newGlyph);
            var error = '';
            glyph.glyph = glyph.glyph.trim();
            glyph.editPass = glyph.editPass.trim();
            glyph.submittedBy = glyph.submittedBy.trim();

            if(glyph.glyph === '') {
                error += '- You must supply a glyph!\n';
            }
            if(glyph.editPass === '') {
                error += '- You must supply a password!\n';
            }
            if(glyph.submittedBy === '') {
                error += '- You must supply a name!\n';
            }

            if(error !== '') {
                alert(error);
                return;
            }
            else {

                glyph.chaliceId = glyph.chalice.value;
                var chaliceName = glyph.chalice.name;
                delete glyph.chalice;
                //$log.log(glyph);
                $glyphService.addGlyph(glyph)
                    .success(function(data, status, headers, config) {
                        var addMsg = data[0][0].Message;
                        if(addMsg === "Glyph added!") {
                            glyph.glyphId = data[0][0].id;
                            $global.current().glyph = glyph;
                            $global.current().glyph.chaliceName = chaliceName;
                            $global.current().editPass = glyph.editPass;
                            $global.current().submittedBy = glyph.submittedBy;
                            $global.current().justAdded = 'Glyph';
                            //$global.dump();
                            $state.transitionTo('AddGlyphFeature');
                        }
                        else {
                            // glyph already found
                            //$log.log(data[0][0].id);
                            $global.current().glyph = {};
                            $global.current().glyph.GlyphId = data[0][0].id;
                            alert('Glyph already added!');
                            $state.transitionTo('GlyphDetail');
                        }
                    })
                    .error(function(data, status, headers, config) {
                        alert('Alas!  Something has gone wrong.  Try again later.');
                    });
            }
        };

    }

    angular.module('chalice.controllers')
        .config(function($stateProvider) {
            $stateProvider
                .state('AddNewGlyph', {
                    url: '/addnewglyph',
                    templateUrl: 'views/AddNewGlyph/addNewGlyph.html'
                });
        })
        .controller('AddNewGlyphController',
        ['$state', 'Log', 'GlobalState', 'ChaliceService', 'GlyphService', AddNewGlyphController]);

})();