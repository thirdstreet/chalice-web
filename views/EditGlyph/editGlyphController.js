/**
 * Created by monk on 6/6/15.
 */
(function() {

    function EditGlyphController($rootScope, $state, $log, $global, $chaliceService, $glyphService) {

        $log.module = 'EditGlyph';
        var _this = this;

        this.glyph = {};
        this.chalices = [];

        $glyphService.getById($global.current().glyph.GlyphId).then(function(response) {
            _this.glyph = response.data.glyph;
            $global.current().glyph = angular.copy(_this.glyph);

            $log.log(_this.glyph);

            if(_this.glyph === undefined) {
                $state.transitionTo('GlyphsByFeature');
                return;
            }

            $chaliceService.getRoot().then(function(response) {
                //$log.log(response.data);
                angular.forEach(response.data, function(c) {
                    var chalice = {
                        value: c.chaliceId,
                        areaName: c.areaName,
                        name: c.chaliceName
                    };
                    _this.chalices.push(chalice);
                });
                _this.glyph.chalice = findChalice(_this.glyph.chaliceId);
                //$log.log(_this.chalices[0]);
            });

        });




        this.cancel = function() {
            $global.current().glyph.GlyphId = _this.glyph.glyphId;
            var previous = $rootScope.previousState;
            $state.transitionTo(previous);
        };

        this.goToFeatures = function() {
            $global.current().glyph.GlyphId = _this.glyph.glyphId;
            $state.transitionTo('AddGlyphFeature');
        };

        this.doEdit = function() {
            var editGlyph = {};//angular.copy(_this.glyph);
            var error = '';
            editGlyph.id = _this.glyph.glyphId;
            editGlyph.glyph = _this.glyph.glyph.trim();
            editGlyph.editPass = _this.glyph.editPass.trim();
            editGlyph.submitter = _this.glyph.submitter.trim();

            if(editGlyph.glyph === '') {
                error += '- You must supply a glyph!\n';
            }
            if(editGlyph.editPass === '') {
                error += '- You must supply a password!\n';
            }
            if(editGlyph.submitter === '') {
                error += '- You must supply a name!\n';
            }

            if(error !== '') {
                alert(error);
                return;
            }
            else {

                editGlyph.chaliceId = _this.glyph.chalice.value;
                editGlyph.isFetid = _this.glyph.isFetid === true || _this.glyph.isFetid === 1 ? 1 : 0;
                editGlyph.isRotted = _this.glyph.isRotted === true || _this.glyph.isRotted === 1 ? 1 : 0;
                editGlyph.isCursed = _this.glyph.isCursed === true || _this.glyph.isCursed === 1 ? 1 : 0;
                editGlyph.isSinister = _this.glyph.isSinister === true || _this.glyph.isSinister === 1 ? 1 : 0;
                //$log.log(editGlyph);

                $glyphService.editGlyph(editGlyph)
                    .success(function(data, status, headers, config) {
                        var editMsg = data[0][0].Message;
                        if(editMsg === 'Glyph updated!') {
                            $global.current().glyph = editGlyph;
                            $global.current().glyph.glyphId = data[0][0].id;
                            $global.current().glyph.GlyphId = data[0][0].id;
                            alert(editMsg);
                            $state.transitionTo('AddGlyphFeature');
                        }
                        else if(editMsg === 'Password incorrect!') {
                            alert('Password incorrect.  Glyph NOT updated.');
                        }
                        else {
                            alert('Glyph not found!  Sure it still exists?');
                        }
                    })
                    .error(function(data, status, headers, config) {
                        alert('Alas!  Something has gone wrong.  Try again later.');
                    });

            }
        };

        function findChalice(id) {
            for(var i = 0; i < _this.chalices.length; i++) {
                if(_this.chalices[i].value === id) {
                    return _this.chalices[i];
                }
            }
            return _this.chalices[0];
        }

    }

    angular.module('chalice.controllers')
        .config(function($stateProvider) {
            $stateProvider
                .state('EditGlyph', {
                    url: '/editglyph',
                    templateUrl: 'views/EditGlyph/editGlyph.html'
                });
        })
        .controller('EditGlyphController',
        ['$rootScope', '$state', 'Log', 'GlobalState', 'ChaliceService', 'GlyphService', EditGlyphController]);

})();