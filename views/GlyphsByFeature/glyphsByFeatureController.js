/**
 * Created by damonfloyd on 6/2/15.
 */
(function() {

    function GlyphsByFeatureController($state, $log, $global, $featureService) {
        $log.module = 'GlyphsByFeatureController';
        var _this = this;
        this.featureList = [];
        this.featureList.push({value: -1, group: '', name: '- Select -'});

        this.feat1 = this.featureList[0];
        this.feat2 = this.featureList[0];
        this.feat3 = this.featureList[0];
        this.feat4 = this.featureList[0];

        $featureService.getAll().then(function(response) {
            var feats = response.data[0];
            angular.forEach(feats, function(f) {
                var opt = {
                    value: f.FeatureId,
                    group: f.TypeName,
                    name: f.FeatureName
                };
                _this.featureList.push(opt);
            });
            if($global.current().searchType === 'GlyphsByFeature') {
                var prevSearch = $global.current().searchArray;
                for (var i = 0; i < prevSearch.length; i++) {
                    var prop = "feat" + (i + 1);
                    _this[prop] = findPreviousSearchItem(prevSearch[i].value);
                }
            }
        });

        this.reset = function(form) {
            if(form) {
                form.$setPristine();
                form.$setUntouched();
            }
            _this.feat1 = this.featureList[0];
            _this.feat2 = this.featureList[0];
            _this.feat3 = this.featureList[0];
            _this.feat4 = this.featureList[0];
        }

        this.doSearch = function() {
            $global.current().searchArray = [];
            $global.current().searchArray.push(_this.feat1, _this.feat2, _this.feat3, _this.feat4);
            $global.current().searchType = 'GlyphsByFeature';
            $state.transitionTo('GlyphSearchResults');
        }

        function findPreviousSearchItem(itemId) {
            //$log.log("itemId:", itemId);
            for(var i = 0; i < _this.featureList.length; i++) {
                var f = _this.featureList[i];
                //$log.log(f);
                if(f.value === itemId) {
                    return _this.featureList[i];
                }
            }
            return _this.featureList[0];
        }


    }

    angular.module('chalice.controllers')
        .config(function($stateProvider) {
            $stateProvider
                .state('GlyphsByFeature', {
                    url: '/',
                    templateUrl: 'views/GlyphsByFeature/glyphsByFeature.html'
                });
        })
        .controller('GlyphsByFeatureController', ['$state', 'Log', 'GlobalState', 'FeatureService', GlyphsByFeatureController]);


})();