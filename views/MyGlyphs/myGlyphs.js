/**
 * Created by damonfloyd on 7/16/15.
 */
(function() {

    function MyGlyphsController($state, $log, $global) {


        this.submitterName = $global.current().submittedBy || '';

        this.go = function() {
            $global.current().submittedBy = this.submitterName;
            $global.current().searchType = 'MyGlyphs';
            $state.transitionTo('GlyphSearchResults');
        }
    }

    angular.module('chalice.controllers')
        .config(function($stateProvider) {
            $stateProvider
                .state('MyGlyphs', {
                    url: '/myglyphs',
                    templateUrl: 'views/MyGlyphs/myGlyphs.html'
                }
            );
        })
        .controller('MyGlyphsController',
        ['$state', 'Log', 'GlobalState', MyGlyphsController]);

})();