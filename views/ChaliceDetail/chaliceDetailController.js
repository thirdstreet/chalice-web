/**
 * Created by damonfloyd on 6/2/15.
 */
(function() {

    function ChaliceDetailController($state, $log, $global, $chaliceService) {
        $log.module = 'ChaliceDetailController';
        //$log.log('hi');

        var _this = this;
        var chaliceId = $global.current().chalice.chaliceId || 11;  // TODO: remove default

        $chaliceService.getById(chaliceId).then(function(response) {
            _this.chalice = response.data[0];
            //$log.log(_this.chalice);
        });

        this.showAreaChalices = function(areaId) {
            $global.areaId = areaId;
            $state.transitionTo('ChalicesByArea');
        };

        this.showGlyphsForChalice = function(chaliceId) {
            $global.current().chalice.chaliceId = chaliceId;
            $global.current().searchType = 'ByChaliceId';
            $state.transitionTo('GlyphSearchResults');
        }
    }

    angular.module('chalice.controllers')
        .config(function($stateProvider) {
            $stateProvider
                .state('ChaliceDetail', {
                    url: '/chalice',
                    templateUrl: 'views/ChaliceDetail/chaliceDetail.html'
                });
        })
        .controller('ChaliceDetailController', ['$state', 'Log', 'GlobalState', 'ChaliceService', ChaliceDetailController]);

})();