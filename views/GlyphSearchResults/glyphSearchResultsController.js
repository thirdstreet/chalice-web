/**
 * Created by damonfloyd on 6/3/15.
 */
(function() {

    function GlyphSearchResultsController($rootScope, $state, $log, $global, $glyphService) {
        $log.module = 'Search';

        var _this = this;
        this.searchString = 'Search Results';
        this.chaliceName = '';
        this.chaliceId = '';

        var searchType = $global.current().searchType;
        this.searchResults = [];
        switch(searchType) {
            case 'ByChaliceId':
                _this.searchString = 'Glyphs By Chalice';
                searchByChaliceId($global.current().chalice.chaliceId);
                break;
            case 'GlyphsByFeature':
                _this.searchString = 'Glyphs By Feature';
                searchByFeatures($global.current().searchArray);
                break;
            case 'GlyphsByBoss':
                _this.searchString = 'Glyphs By Boss';
                searchByBosses($global.current().searchArray);
                break;
            case 'RecentGlyphs':
                _this.searchString = 'Most Recent Glyphs';
                getRecentGlyphs();
                break;
            case 'GlyphsBySubmitter':
                _this.searchString = 'Glyphs By Submitter';
                searchBySubmitter($global.current().submitterSearch);
                break;
            case 'MyGlyphs':
                _this.searchString = 'My Glyphs';
                searchBySubmitter($global.current().submittedBy);
                break;
        }

        this.showChaliceDetail = function(chaliceId) {
            $global.current().chalice.chaliceId = chaliceId;
            $state.transitionTo('ChaliceDetail');
        };

        this.viewGlyph = function(glyphId) {
            $global.current().glyph.GlyphId = glyphId;
            $global.dump();
            $state.transitionTo('GlyphDetail');
        };

        this.backToSearch = function() {
            var previous = $rootScope.previousState;
            $state.transitionTo(previous);
        };

        function searchBySubmitter(submitter) {
            var post = { submitter : submitter };
            $glyphService.getBySubmitter(post).then(function(response) {
                _this.searchResults = formatGlyphs(response.data);
                //$log.log(response.data);
            });
        };

        function getRecentGlyphs() {
            $glyphService.getRecent().then(function(response) {
                _this.searchResults = formatGlyphs(response.data);
            });
        }

        function searchByFeatures(featureArray) {
            var post = {};
            for(var i = 0; i < featureArray.length; i++) {
                var feature = featureArray[i];
                if(feature.value !== -1) {
                    var prop = 'feat' + (i + 1);
                    post[prop] = feature.value;
                }
            }
            //$log.log(post);
            $glyphService.getByFeatures(post).then(function(response) {
                _this.searchResults = formatGlyphs(response.data);
            });
        }

        function searchByBosses(bossArray) {
            var post = {};
            for(var i = 0; i < bossArray.length; i++) {
                var boss = bossArray[i];
                //$log.log(boss);
                if(boss.value !== -1) {
                    var prop = 'boss' + (i + 1);
                    post[prop] = boss.value;
                }
            }
            $log.log(post);
            $glyphService.getByBosses(post).then(function(response) {
                _this.searchResults = formatGlyphs(response.data);
            });
        }

        function searchByChaliceId(chaliceId) {
            var cId = chaliceId || 11;
            $glyphService.getByChaliceId(cId).then(function (response) {
                _this.searchResults = formatGlyphs(response.data);
                _this.chaliceName = _this.searchResults[0].chaliceName;
                _this.chaliceId = cId;
                //$log.log(_this.searchResults);
            });
        }


        function formatGlyphs(glyphList) {
            if(!glyphList.hasOwnProperty('message')) {
                angular.forEach(glyphList, function (glyph) {
                    var weapCount = 0;
                    var runeCount = 0;
                    var upgradeCount = 0;
                    var materialCount = 0;
                    if (glyph.features.length !== 0) {
                        angular.forEach(glyph.features, function (feature) {
                            if (feature.featureType === "Weapon") {
                                weapCount++;
                            }
                            else if (feature.featureType === "Caryll Rune") {
                                runeCount++;
                            }
                            else if (feature.featureType === "Upgrade Material") {
                                upgradeCount++;
                            }
                            else if (feature.featureType === "Ritual Material") {
                                materialCount++;
                            }

                            //glyph.dateAdded = moment(new Date(glyph.dateAdded)).format('YYYY.MM.DD')

                        });
                    }
                    glyph.weaponCount = weapCount;
                    glyph.runeCount = runeCount;
                    glyph.upgradeCount = upgradeCount;
                    glyph.materialCount = materialCount;
                    glyph.workingPercentage = computeWorkingPercentage(glyph.workingVotes, glyph.nonWorkingVotes);

                    //$log.log(glyph);
                });
            }
            return glyphList;
        }
    }

    function computeWorkingPercentage(working, nonworking) {
        var total = working + nonworking;
        var percentage = (working / total) * 100;
        if(isNaN(percentage)) {
            percentage = 0;
        }
        return percentage;
    }

    angular.module('chalice.controllers')
        .config(function($stateProvider) {
            $stateProvider
                .state('GlyphSearchResults', {
                    url: '/glyphresults',
                    templateUrl: 'views/GlyphSearchResults/glyphSearchResults.html'
                });
        })
        .controller('GlyphSearchResultsController'
            ,['$rootScope', '$state', 'Log', 'GlobalState', 'GlyphService', GlyphSearchResultsController]);

})();