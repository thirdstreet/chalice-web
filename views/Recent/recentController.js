/**
 * Created by damonfloyd on 6/1/15.
 */
(function() {

    function RecentController($state, $log, $global, $glyphService) {
        this.recent = [];

        $log.module = 'RecentController';
        //$log.log('here we are...');

        var _this = this;

        $global.current().searchType = 'RecentGlyphs';
        $state.transitionTo('GlyphSearchResults');

        /*

        $glyphService.getRecent().then(function(data) {
            _this.recent = data.data.rows;
            //angular.forEach(_this.recent, function(glyph) {
            //    glyph.DateAdded = moment(glyph.DateAdded).format('YYYY.MM.DD.HH.MM.ss');
            //});
        });

        this.viewGlyph = function(glyphId) {
            $global.current().glyph.GlyphId = glyphId;
            $global.dump();
            $state.transitionTo('GlyphDetail');
        };

        */
    }

    angular.module('chalice.controllers')
        .config(function($stateProvider) {
            $stateProvider
                .state('recent', {
                    url: '/recent',
                    templateUrl: 'views/Recent/recent.html'
                });
        })
        .controller('RecentController',
            [
                '$state',
                'Log',
                'GlobalState',
                'GlyphService',
                RecentController
            ]
    );

})();