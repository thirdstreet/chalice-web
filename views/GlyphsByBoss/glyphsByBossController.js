/**
 * Created by damonfloyd on 6/2/15.
 */
(function() {

    function GlyphsByBossController($state, $log, $global, $bossService) {
        $log.module = 'GlyphsByBossController';
        var _this = this;
        this.bossList = [];
        this.bossList.push({value: -1, name: '- Select -'});

        this.boss1 = this.bossList[0];
        this.boss2 = this.bossList[0];
        this.boss3 = this.bossList[0];
        this.boss4 = this.bossList[0];

        $bossService.getAll().then(function(response) {
            var bosses = response.data[0];
            //$log.log(bosses);
            angular.forEach(bosses, function(b) {
                var opt = {
                    value: b.id,
                    name: b.name
                };
                _this.bossList.push(opt);
            });
            if($global.current().searchType === 'GlyphsByBoss') {
                var prevSearch = $global.current().searchArray;
                for (var i = 0; i < prevSearch.length; i++) {
                    var prop = "boss" + (i + 1);
                    _this[prop] = findPreviousSearchItem(prevSearch[i].value);
                }
            }
        });

        this.reset = function(form) {
            if(form) {
                form.$setPristine();
                form.$setUntouched();
            }
            _this.boss1 = this.bossList[0];
            _this.boss2 = this.bossList[0];
            _this.boss3 = this.bossList[0];
            _this.boss4 = this.bossList[0];
        }

        this.doSearch = function() {
            $global.current().searchArray = [];
            $global.current().searchArray.push(_this.boss1, _this.boss2, _this.boss3, _this.boss4);
            $global.current().searchType = 'GlyphsByBoss';
            $state.transitionTo('GlyphSearchResults');
        }

        function findPreviousSearchItem(itemId) {
            //$log.log("itemId:", itemId);
            for(var i = 0; i < _this.bossList.length; i++) {
                var f = _this.bossList[i];
                //$log.log(f);
                if(f.value === itemId) {
                    return _this.bossList[i];
                }
            }
            return _this.bossList[0];
        }


    }

    angular.module('chalice.controllers')
        .config(function($stateProvider) {
            $stateProvider
                .state('GlyphsByBoss', {
                    url: '/',
                    templateUrl: 'views/GlyphsByBoss/glyphsByBoss.html'
                });
        })
        .controller('GlyphsByBossController', ['$state', 'Log', 'GlobalState', 'BossService', GlyphsByBossController]);


})();