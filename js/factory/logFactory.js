/**
 * Created by damonfloyd on 6/1/15.
 */

(function() {
    function LogFactory() {
        this.module = "ANON! (name this, yo!)";
        this.debug = true;
        this.showModule = true;
        var _this = this;

        this.log = function () {
            if (this.debug) {

                console.group(prepend());
                console.log(arguments);
                console.groupEnd();
            }
        };

        this.error = function () {
            if(this.debug) {
                console.group(prepend());
                console.error(arguments);
                console.groupEnd();
            }
        };

        this.warn = function () {
            if(this.debug) {
                console.group(prepend());
                console.warn(arguments);
                console.groupEnd();
            }
        };

        function getTimeStamp() {
            return moment().format('HH:MM:ss');
        }

        function prepend() {
            var prependStr = getTimeStamp();
            if (_this.showModule) {
                prependStr += " " + _this.module;
            }
            return prependStr;
        }

        return this;
    }

    angular.module('chalice')
        .factory('Log', LogFactory);

})();