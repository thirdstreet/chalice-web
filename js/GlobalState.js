/**
 * Created by damonfloyd on 6/1/15.
 */
(function() {

    function GlobalState($log) {

        var currentItems = {};
        $log.module = 'GlobalState';

        currentItems.chalice = {};
        currentItems.glyph = {};
        currentItems.editPass = '';
        currentItems.submittedBy = '';
        currentItems.areaId = '';
        currentItems.searchType = 'ByChaliceId';
        currentItems.searchArray = [];
        currentItems.justAdded = '';
        currentItems.submitterSearch = '';

        return {
            current: function() {
                return currentItems;
            },

            clear: function() {
                currentItems.chalice = {};
                currentItems.glyph = {};
                currentItems.editPass = '';
                currentItems.submittedBy = '';
                currentItems.areaId = '';
                currentItems.searchType = 'ByChaliceId';
                currentItems.searchArray = [];
                currentItems.justAdded = '';
                currentItems.submitterSearch = '';
            },

            dump: function() {
                $log.log(currentItems);
            }
        };
    }

    angular.module('chalice')
        .factory('GlobalState', ['Log', GlobalState]);
})();