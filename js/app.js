/**
 * Created by damonfloyd on 6/1/15.
 */

angular.module('chalice.controllers', []);
angular.module('chalice.services', []);
angular.module('chalice.states', []);

angular.module('chalice', [
    'ui.router',
    'ngResource',
    'chalice.controllers',
    'chalice.services',
    'chalice.states'
])
    .filter('unsafe', function($sce) {
        return function(val) {
            return $sce.trustAsHtml(val);
        };
    })
    .run(function($rootScope) {
        $rootScope.previousState;
        $rootScope.currentState;
        $rootScope.$on('$stateChangeSuccess', function(ev, to, toParams, from, fromParams) {
            $rootScope.previousState = from.name;
            $rootScope.currentState = to.name;
            //console.log('Previous state:'+$rootScope.previousState)
            //console.log('Current state:'+$rootScope.currentState)
        });

        //console.log('we are running...');
        $(document).foundation({
            offcanvas: {
                close_on_click: true
            }
        });
    })
    .constant('ServiceUrl', 'http://192.168.108.184:40704' )
    //.constant('ServiceUrl', 'http://service.chalicedungeonglyphs.com' )
    .constant('version', '0.1')
    .config(function($urlRouterProvider, version) {
        $urlRouterProvider.otherwise("/");
    });