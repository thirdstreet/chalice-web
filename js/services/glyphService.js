/**
 * Created by damonfloyd on 6/1/15.
 */
(function() {

    function GlyphService($http, $log, ServiceUrl) {
        var baseUrl = ServiceUrl + '/glyph';
        this.getRecent = function() {
            return $http.get(baseUrl + '/recent');
        };

        this.getByChaliceId = function(chaliceId) {
            return $http.get(baseUrl + '/chalice/' + chaliceId);
        };

        this.getById = function (glyphId) {
            //$log.module = 'GlyphService::getById';
            //$log.log(baseUrl + '/' + glyphId);
            return $http.get(baseUrl + '/' + glyphId);
        };

        this.getByFeatures = function(features) {
            return $http.post(baseUrl + '/feature', features);
        };

        this.addGlyph = function(newGlyph) {
            return $http.post(baseUrl, newGlyph);
        };

        this.editGlyph = function(glyph) {
            return $http.put(baseUrl + '/' + glyph.id, glyph);
        };

        this.addFeatureToGlyph = function(glyphId, feature) {
            //$log.log(glyphId, feature);
            return $http.post(baseUrl + '/' + glyphId + '/feature', feature);
        };

        this.voteOnGlyph = function(glyphId, vote) {
            //$log.log(glyphId, vote);
            return $http.put(baseUrl + '/' + glyphId + '/' + vote);
        };

        this.getBySubmitter = function(submitter) {
            return $http.post(baseUrl + '/submitter', submitter);
        };

        this.getBosses = function(glyphId) {
            return $http.get(baseUrl + '/' + glyphId + '/boss');
        };

        this.getByBosses = function(bosses) {
            return $http.post(baseUrl + '/boss', bosses);
        };
    }

    angular.module('chalice.services')
        .service('GlyphService', ['$http', 'Log', 'ServiceUrl', GlyphService]);

})();