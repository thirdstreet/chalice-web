/**
 * Created by damonfloyd on 8/13/15.
 */
(function() {

    function BossService($http, $log, ServiceUrl) {
        var baseUrl = ServiceUrl + '/boss';
        $log.module = 'BossService';

        this.getAll = function() {
            //$log.log(baseUrl + '/');
            return $http.get(baseUrl + '/');
        };

        this.glyphBosses = function(glyphId) {
            return $http.get(baseUrl + '/' + glyphId);
        };

        this.deleteGlyphBoss = function(bossId, postData) {
            $log.log(bossId, postData);
            // this is a PUT because angular doesn't allow a body in a DELETE request.  (no idea why)
            return $http.put(baseUrl + '/' + bossId, postData);
        };
    }

    angular.module('chalice.services')
        .service('BossService', ['$http', 'Log', 'ServiceUrl', BossService]);

})();