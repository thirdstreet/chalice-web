/**
 * Created by damonfloyd on 6/1/15.
 */
(function() {

    function ChaliceService($http, $log, ServiceUrl) {
        var baseUrl = ServiceUrl + '/chalice';
        this.getAll = function() {
            return $http.get(baseUrl);
        };

        this.getRoot = function() {
            return $http.get(baseUrl + '/root');
        };

        this.getById = function(chaliceId) {
            return $http.get(baseUrl + '/' + chaliceId);
        };

        this.getByArea = function(areaId) {
            return $http.get(baseUrl + '/area/' + areaId);
        };

        this.getByMaterials = function(materials) {
            return $http.post(baseUrl + '/materials', materials);
        };

        this.getWithGlyphs = function() {
            return $http.get(baseUrl + '/glyph');
        };

    }

    angular.module('chalice.services')
        .service('ChaliceService', ['$http', 'Log', 'ServiceUrl', ChaliceService]);

})();