/**
 * Created by damonfloyd on 6/4/15.
 */
(function() {

    function FeatureService($http, $log, ServiceUrl) {
        var baseUrl = ServiceUrl + '/feature';
        $log.module = 'FeatureService';

        this.getAll = function() {
            $log.log(baseUrl + '/');
            return $http.get(baseUrl + '/');
        };

        this.getByTypeId = function(typeId) {
            return $http.get(baseUrl + '/type/' + typeId);
        };

        this.glyphFeatures = function(glyphId) {
            return $http.get(baseUrl + '/' + glyphId);
        };

        this.deleteGlyphFeature = function(featureId, postData) {
            $log.log(featureId, postData);
            // this is a PUT because angular doesn't allow a body in a DELETE request.  (no idea why)
            return $http.put(baseUrl + '/' + featureId, postData);
        };
    }

    angular.module('chalice.services')
        .service('FeatureService', ['$http', 'Log', 'ServiceUrl', FeatureService]);

})();