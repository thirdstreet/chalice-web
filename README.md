# README #

Chalice-Web - 

This code is explicitly styled to work on mobile devices.  It is semi-responsive, but that is only by the grace of Foundation.  Running on non-mobile (or non-emulated mobile) is gonna look funny.  Fully responsive is on the list.  It's just pretty far DOWN on the list.  YMMV.


### What is this repository for? ###

* Website code for chalicedungeonglyphs.com - needs chalice-api for a backend service.

### How do I get set up? ###

* install chalice-api (different repo)
* update service url in app.js to point to your service instance.
* ...
* profit?

### Contribution guidelines ###

* submit a pull request, I guess.  Wasn't really planning on this being collaborative.  ;)

### Who do I talk to? ###

* Damon Floyd <dfloyd@cowtao.com> - or -
* /u/Monkrocker on reddit.  (I pretty much only post in /r/Bloodborne)

Thanks for stopping by!